<p><?php echo $subscriber_name; ?>,</p>

<p>
<?php echo sprintf( __( 'A new post, <a href="%1$s">%2$s</a>, by %3$s (%4$s), has been published.', EmailAlerts::PLUGIN_NAME ),
	$post_permalink, $post->post_title, $post_author_name, $post_author_email ); ?>
</p>

<?php
	if( $post_author_email != $acting_admin_email)
	{
		echo '<p>';
		echo sprintf( __( 'Approved by: %1$s (%2$s)',EmailAlerts::PLUGIN_NAME ), $acting_admin_name, $acting_admin_email );
		echo '</p>p>';
	}
?>

<p><br/>
	<small>Vous pouvez changer vos abonnements dans votre profil sur <a href="<?php echo $site_url.'/wp-admin/profile.php'; ?>"><?php echo $site_url; ?></a> </small>
</p>
