<?php echo $subscriber_name; ?>,<br /><br />

<?php echo sprintf( __( 'A comment by %2$s (%3$s) on <a href="%5$s">"%4$s"</a> has been published on %1$s.', EmailAlerts::PLUGIN_NAME ), 
	$blog_name, $comment_author_name, $comment_author_email, $post->post_title, get_permalink($comment->comment_post_ID) ); ?><br /><br />
	
<?php echo sprintf( __( '<a href="%1$s">View the comment</a>.', EmailAlerts::PLUGIN_NAME ), $comment_url ); ?><br /><br />

<?php if ( isset( $acting_admin_name ) ) echo sprintf( __( 'Approved by: %1$s (%2$s)', EmailAlerts::PLUGIN_NAME ), $acting_admin_name, $acting_admin_email ); ?>

<p><small><?php _e( '(Email Alerts plugin)', EmailAlerts::PLUGIN_NAME ); ?></small></p>
