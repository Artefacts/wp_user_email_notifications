��            )   �      �     �     �     �     �  $   �          0  Q   F  H   �  m   �  	   O     Y     r     �  	   �     �      �  2   �  /     "   >  #   a     �     �  B   �  V   �  C   7     {     �  �  �     �     �     �  (   �  '   �     %	     @	  :   X	  L   �	  ~   �	     _
     g
     �
     �
     �
     �
  #   �
  8   �
  3   0  (   d  @   �     �     �  F   �  @   7  H   x  *   �  &   �                        
                                                     	                                                             %1$s: Comment approved %1$s: Comment awaiting approval %1$s: Post published (Email Alerts plugin) <a href="%1$s">View the comment</a>. <a href="%s">Delete</a> <a href="%s">Edit</a> A comment by %2$s (%3$s) on <a href="%5$s">"%4$s"</a> has been published on %1$s. A new post, <a href="%1$s">%2$s</a>, by %3$s (%4$s), has been published. Allows users to configure whether they wish to be alerted when a comment is added, comment held, post posted. Anonymous Approved by: %1$s (%2$s) Author: %1$s (%2$s , %3$s) Comment notifications Comment:  Email Alerts How many widgets would you like? Notify me when comments are approved onto the site Notify me when comments are held for moderation Notify me when posts are published Notify me when posts require review Post notifications Save There is a comment on <a href="%2$s">"%1$s"</a> awaiting approval. There is a new pingback pending approval. The pingback is on <a href="%2$s">"%1$s"</a> There is a new trackback pending approval <a href="%2$s">"%1$s"</a> URL: <a href="%1$s">%1$s</a> Unapproved by: %1$s (%2$s) Project-Id-Version: email-alerts
Report-Msgid-Bugs-To: http://wordpress.org/tag/email-alerts
POT-Creation-Date: 2010-05-13 17:14+0000
PO-Revision-Date: 2015-09-09 21:28+0100
Last-Translator: Cyrille37 <Cyrille37@gmail.com>
Language-Team: Cyrille <cyrille37@free.fr>
Language: fr-FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop
X-Poedit-Basepath: .
 %1$s: Commentaire approuvé %1$s: Commentaire à valider %1$s: Article publié (Plugin alertes par email: Email Alerts) <a href="%1$s">Voir le commentaire</a>. <a href="%s">Supprimer</a> <a href="%s">Editer</a> %2$s (%3$s) a commenté <a href="%5$s">"%4$s"</a> le %1$s. Un nouvel article a été publié par %3$s (%4$s) : <a href="%1$s">%2$s</a>. Permet aux utilisateurs de configurer de recevoir ou non des alertes par email quand un article ou un commentaire est publié. Anonyme Approuvé par: %1$s (%2$s) Auteur: %1$s (%2$s , %3$s) Notification commentaires Commentaire:  Alertes email Combien de widgets souhaitez vous ? Me notifier quand un commentaire est validé sur le site Me notifier quand un commentaire attend modération Me notifier quand un article est publié Me notifier quand un article doit être validé pour publication Notification articles Enregistrer Un commentaire sur <a href="%2$s">"%1$s"</a> attend d'être approuvé. Un pingback en attente de modération <a href="%2$s">"%1$s"</a>. Il y a un trackback en attente de modération <a href="%2$s">"%1$s"</a>. Adresse web (URL): <a href="%1$s">%1$s</a> Modération refusée par : %1$s (%2$s) 